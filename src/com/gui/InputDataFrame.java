package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CompanyNode;
import com.servercommunication.graphobjects.ContactHistoryNode;
import com.servercommunication.graphobjects.CooperationSummaryNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;

public class InputDataFrame extends JFrame {

    //panel dodawania rekordu z pustymi polami
    public InputDataFrame() {
        super("Utwórz nową firmę");
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);
        initGraphicalComponents();
        this.requestFocus();
        this.setResizable(false);
        this.pack();
        this.setIconImage(GlobalOptions.img.getImage());

    }

    //panel edycji rekordu
    public InputDataFrame(CompanyNode node) {
        super("Szczegóły firmy");

        this.companyNode = node;
        this.id = node.getId();
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);
        initGraphicalComponents(node);

        this.requestFocus();
        this.setResizable(true);
        this.setIconImage(GlobalOptions.img.getImage());
    }

    private void initGraphicalComponents() {
        mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.add(mainPanel, BorderLayout.CENTER);
        mainPanel.setLayout(new BorderLayout());
        generalInfoPanel = new JPanel();
        descriptionPanel = new JPanel();
        controlPanel = new JPanel();

        mainPanel.add(generalInfoPanel, BorderLayout.WEST);
        nameField = new JTextField(30);
        emailField = new JTextField(30);
        phoneField = new JTextField(30);
        wwwField = new JTextField(30);
        potField = new JTextField(30);

        generalInfoPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        generalInfoPanel.setLayout(new GridLayout(10, 1));
        generalInfoPanel.add(new JLabel("Nazwa"));
        generalInfoPanel.add(nameField);
        generalInfoPanel.add(new JLabel("E-mail"));
        generalInfoPanel.add(emailField);
        generalInfoPanel.add(new JLabel("Numer telefonu"));
        generalInfoPanel.add(phoneField);
        generalInfoPanel.add(new JLabel("Strona internetowa"));
        generalInfoPanel.add(wwwField);
        generalInfoPanel.add(new JLabel("Potencjał(liczba!)"));
        generalInfoPanel.add(potField);

        descriptionPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mainPanel.add(descriptionPanel, BorderLayout.CENTER);
        descArea = new JTextArea(10, 30);

        descriptionPanel.setLayout(new BorderLayout());
        descriptionPanel.add(new JLabel("Opis"), BorderLayout.NORTH);
        descriptionPanel.add(new JScrollPane(descArea), BorderLayout.CENTER);

        mainPanel.add(controlPanel, BorderLayout.SOUTH);
        buttonCancel = new JButton("Anuluj");
        buttonEdit = new JButton("Edytuj");
        buttonSave = new JButton("Zapisz");
        buttonSetResponsibility = new JButton("Wyznacz osobę odpowiedzialną");

        controlPanel.setLayout(new FlowLayout());
        controlPanel.add(buttonCancel);
        buttonCancel.addActionListener(this::actionPerformed);
        controlPanel.add(buttonSave);
        buttonSave.addActionListener(this::actionPerformed);

        buttonEdit.setEnabled(false);
    }

    private void initGraphicalComponents(CompanyNode node) {

        mainPanel = new JPanel();
        this.add(mainPanel, BorderLayout.WEST);
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mainPanel.setLayout(new BorderLayout());

        generalInfoPanel = new JPanel();
        descriptionPanel = new JPanel();
        controlPanel = new JPanel();
        // TODO MAKE THIS IN GRID PANEL START
        notesPanel = new JPanel();
        noteButtonPanel = new JPanel();
        summaryPanel = new JPanel();
        summaryButtonPanel = new JPanel();
        // TODO MAKE THIS IN GRID PANEL END

        mainPanel.add(generalInfoPanel, BorderLayout.WEST);
        nameField = new JTextField(node.getName(), 30);
        emailField = new JTextField(node.getEmail(), 30);
        phoneField = new JTextField(node.getPhoneNumber(), 30);
        wwwField = new JTextField(node.getWww(), 30);
        potField = new JTextField(Integer.toString(node.getPotential()), 30);
        responsibleField = new JTextField();
        resetResponsibleField();
        responsibleField.setEditable(false);

        generalInfoPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        generalInfoPanel.setLayout(new GridLayout(12, 1));
        generalInfoPanel.add(new JLabel("Nazwa"));
        generalInfoPanel.add(nameField);
        generalInfoPanel.add(new JLabel("E-mail"));
        generalInfoPanel.add(emailField);
        generalInfoPanel.add(new JLabel("Numer telefonu"));
        generalInfoPanel.add(phoneField);
        generalInfoPanel.add(new JLabel("Strona internetowa"));
        generalInfoPanel.add(wwwField);
        generalInfoPanel.add(new JLabel("Potencjał(liczba!)"));
        generalInfoPanel.add(potField);
        generalInfoPanel.add(new JLabel("Osoba odpowiedzialna"));
        generalInfoPanel.add(responsibleField);

        descriptionPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mainPanel.add(descriptionPanel, BorderLayout.CENTER);

        try {
            descArea = new JTextArea(URLDecoder.decode(node.getDescription(), StandardCharsets.UTF_8.toString()), 10, 30);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }

        descArea.setLineWrap(true);

        descriptionPanel.setLayout(new BorderLayout());
        descriptionPanel.add(new JLabel("Opis"), BorderLayout.NORTH);
        descriptionPanel.add(new JScrollPane(descArea), BorderLayout.CENTER);

        mainPanel.add(controlPanel, BorderLayout.SOUTH);
        buttonCancel = new JButton("Anuluj");
        buttonEdit = new JButton("Edytuj");
        buttonSave = new JButton("Zapisz");
        buttonSetResponsibility = new JButton("Wyznacz osobę odpowiedzialną");

        controlPanel.setLayout(new FlowLayout());
        controlPanel.add(buttonCancel);
        buttonCancel.addActionListener(this::actionPerformed);
        controlPanel.add(buttonEdit);
        buttonEdit.addActionListener(this::actionPerformed);
        controlPanel.add(buttonSave);
        buttonSave.addActionListener(this::actionPerformed);
        controlPanel.add(buttonSetResponsibility);
        buttonSetResponsibility.addActionListener(this::actionPerformed);

        containerPanel = new JPanel(new GridLayout(1,2));
        this.add(containerPanel, BorderLayout.CENTER);
        // TODO MAKE THIS IN GRID PANEL START
        containerPanel.add(notesPanel);// TODO
        notesPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        notesPanel.setLayout(new BorderLayout());
        notesPanel.add(new JLabel("Dziennik kontaktów"), BorderLayout.NORTH);
        listModelNotes = new JPanel();
        notesScrollPanel = new JScrollPane(listModelNotes);
        notesPanel.add(notesScrollPanel, BorderLayout.CENTER);

        buttonAddNote = new JButton("Dodaj notatkę");
        buttonRefreshNote = new JButton("Odśwież");
        buttonRefreshNote.addActionListener(this::actionPerformed);
        buttonAddNote.addActionListener(this::actionPerformed);

        notesPanel.add(noteButtonPanel, BorderLayout.SOUTH);
        noteButtonPanel.setLayout(new GridLayout());
        noteButtonPanel.add(buttonAddNote);
        noteButtonPanel.add(buttonRefreshNote);

        containerPanel.add(summaryPanel);// TODO
        summaryPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        summaryPanel.setLayout(new BorderLayout());
        summaryPanel.add(new JLabel("Raporty ze współpracy"), BorderLayout.NORTH);
        listModelSummary = new JPanel();
        summaryScrollPanel = new JScrollPane((listModelSummary));
        summaryPanel.add(summaryScrollPanel, BorderLayout.CENTER);

        buttonAddSummary = new JButton("Dodaj raport");
        buttonRefreshSummary = new JButton("Odśwież");
        buttonAddSummary.addActionListener(this::actionPerformed);
        buttonRefreshSummary.addActionListener(this::actionPerformed);

        summaryPanel.add(summaryButtonPanel, BorderLayout.SOUTH);
        summaryButtonPanel.setLayout(new GridLayout());
        summaryButtonPanel.add(buttonAddSummary);
        summaryButtonPanel.add(buttonRefreshSummary);
// TODO MAKE THIS IN GRID PANEL END
        this.pack();
        showNotes();
        showSummary();
        setComponentsInactive();
    }

    private void setComponentsInactive() {
        buttonSave.setEnabled(false);
        nameField.setEditable(false);
        emailField.setEditable(false);
        wwwField.setEditable(false);
        phoneField.setEditable(false);
        potField.setEditable(false);
        descArea.setEditable(false);
    }

    private void setComponentsActive() {
        buttonSave.setEnabled(true);
        nameField.setEditable(true);
        emailField.setEditable(true);
        wwwField.setEditable(true);
        phoneField.setEditable(true);
        potField.setEditable(true);
        descArea.setEditable(true);
    }

    private void actionPerformed(ActionEvent e) {
        if (id != null) {
            if (e.getSource() == buttonEdit)
                setComponentsActive();
            else if (e.getSource() == buttonSave) {
                setComponentsInactive();

                try {
                    ServerCommunication.mutations.companyUpdate(id, nameField.getText(), URLEncoder.encode(descArea.getText(), StandardCharsets.UTF_8.toString()), emailField.getText(), phoneField.getText(), wwwField.getText(), Integer.parseInt(potField.getText()));
                    URLEncoder.encode(descArea.getText(), StandardCharsets.UTF_8.toString());
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException(ex.getCause());
                }


                dispose();
            } else if (e.getSource() == buttonSetResponsibility) {
                new ResponsibilityFrame(id, this).setVisible(true);
            }
            else if(e.getSource() == buttonRefreshNote){
                showNotes();
            }
            else if(e.getSource() == buttonAddNote){
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new NoteFrame(id).setVisible(true);
                    }
                });
            }
            else if(e.getSource() == buttonRefreshSummary){
                showSummary();
            }
            else if(e.getSource() == buttonAddSummary){
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new SummaryFrame(id).setVisible(true);
                    }
                });
            }
        } else {
            if (e.getSource() == buttonSave) {

                try {
                    ServerCommunication.mutations.companyCreate(nameField.getText(), URLEncoder.encode(descArea.getText(), StandardCharsets.UTF_8.toString()), emailField.getText(), phoneField.getText(), wwwField.getText(), Integer.parseInt(potField.getText()));
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException(ex.getCause());
                }
                dispose();
            }
        }
        if (e.getSource() == buttonCancel)
            dispose();
    }

    public void showNotes() {
        while (listModelNotes.getComponentCount() > 0) {
            listModelNotes.remove(listModelNotes.getComponent(listModelNotes.getComponentCount() - 1));
        }
        this.notesHistory = ServerCommunication.queries.contactHistoryByCompany(this.getId());
        this.notesHistory.sort(contactHistoryNodeComparator);

        for (ContactHistoryNode node : notesHistory) {
            listModelNotes.setLayout(new GridLayout(notesHistory.size(), 1));
            listModelNotes.add(new ContactHistoryPanel(node, this));
        }
        this.paintComponents(getGraphics());
    }

    public static Comparator<ContactHistoryNode> contactHistoryNodeComparator = new Comparator<ContactHistoryNode>(){
        public int compare(ContactHistoryNode c1, ContactHistoryNode c2){
            LocalDateTime c1t = LocalDateTime.parse(c1.getContactDate());
            LocalDateTime c2t = LocalDateTime.parse(c2.getContactDate());
            return c2t.compareTo(c1t);
        }
    };

    public void showSummary() {
        while (listModelSummary.getComponentCount() > 0) {
            listModelSummary.remove(listModelSummary.getComponent(listModelSummary.getComponentCount() - 1));
        }
        this.summaryHistory = ServerCommunication.queries.cooperationSummaryByCompany(id);

        for (CooperationSummaryNode node : summaryHistory) {
            listModelSummary.setLayout(new GridLayout(summaryHistory.size(), 1));
            listModelSummary.add(new SummaryPanel(node, this));
            System.out.println(node.getCompany().getName());
        }
        this.paintComponents(getGraphics());
    }

    public String getId(){
        return id;
    }

    public void resetResponsibleField(){
        ArrayList< CompanyNode > companies = ServerCommunication.queries.companiesAll();
        this.responsibleField.setText("Brak");
        for(CompanyNode elem : companies){
            if(elem.getId().equals(id)){
                try{
                    this.responsibleField.setText(elem.getResponsiblePerson().getName() + " " + elem.getResponsiblePerson().getSurname());
                    break;
                }catch (NullPointerException a){
                    break;
                }
            }
        }
    }


    private JPanel mainPanel;
    private JPanel generalInfoPanel;
    private JPanel descriptionPanel;
    private JPanel controlPanel;
    private JPanel containerPanel;
    private JPanel notesPanel;
    private JPanel noteButtonPanel;
    private JPanel summaryPanel;
    private JPanel summaryButtonPanel;

    private JTextArea descArea;

    private JTextField nameField;
    private JTextField emailField;
    private JTextField phoneField;
    private JTextField wwwField;
    private JTextField potField;
    private JTextField responsibleField;

    private JScrollPane notesScrollPanel;
    private JScrollPane summaryScrollPanel;

    private JButton buttonCancel;
    private JButton buttonEdit;
    private JButton buttonSave;
    private JButton buttonSetResponsibility;
    private JButton buttonAddNote;
    private JButton buttonRefreshNote;
    private JButton buttonAddSummary;
    private JButton buttonRefreshSummary;

    private String id;

    private JPanel listModelNotes;
    private JPanel listModelSummary;

    private ArrayList<ContactHistoryNode> notesHistory;
    private ArrayList<CooperationSummaryNode> summaryHistory;
    private CompanyNode companyNode;

}

