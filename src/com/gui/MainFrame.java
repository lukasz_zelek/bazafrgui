package com.gui;

import com.servercommunication.GlobalServerShit;
import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CompanyNode;
import com.servercommunication.graphobjects.StaffNode;
import org.apache.batik.transcoder.TranscoderException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MainFrame extends JFrame {
    public MainFrame(StaffNode me) {
        super(GlobalOptions.superString);
        Toolkit t = Toolkit.getDefaultToolkit();
        this.setBounds(0, 0, t.getScreenSize().width, t.getScreenSize().height - 50);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new BorderLayout());
        this.setVisible(true);
        this.setJMenuBar(mb);
        this.me=me;
        this.setIconImage(GlobalOptions.img.getImage());
        initGraphicalComponents();
    }

    private void initGraphicalComponents() {
        this.add(mainPanel, BorderLayout.NORTH);
        mainPanel.setLayout(new BorderLayout());

        closeProgram.addActionListener(this::actionPerformed);
        programMenu.add(closeProgram);

        about.addActionListener(this::actionPerformed);
        programMenu.add(about);

        addCompany.addActionListener(this::actionPerformed);
        companiesMenu.add(addCompany);

        myCompanies.addActionListener(this::actionPerformed);
        companiesMenu.add(myCompanies);

        showProfile.addActionListener(this::actionPerformed);
        userInfoMenu.add(showProfile);
        logOut.addActionListener(this::actionPerformed);
        userInfoMenu.add(logOut);

        mb.setLayout(new GridLayout(1, 12));
        mb.add(programMenu);
        mb.add(companiesMenu);

        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(userInfoMenu);

        listModel = new JPanel();
        listModel.setLayout(new GridLayout(1000, 1));
        listScrollPane = new JScrollPane(listModel);

        companyContentPanel = new JPanel();
        companyContentPanel.setLayout(new BorderLayout());
        companyContentPanel.add(listScrollPane,BorderLayout.CENTER);
        filterPanel = new FilterPanel(this,listModel);
        companyContentPanel.add(filterPanel,BorderLayout.NORTH);

        this.add(companyContentPanel, BorderLayout.CENTER);

        showMyCompanies();

        profile = new PersonalPanel(me);
        this.add(profile,BorderLayout.EAST);
    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource() == closeProgram)
            System.exit(0);
        if (e.getSource() == about){
            //todo show Integra logo and label
            aboutStuffPanel = new JPanel(new GridLayout(2,1));
            svgfile = new File("logo_trans.svg");
            try {
                iconsvg = new SVGIcon(svgfile.toURI().toString(),600,300);
            } catch (TranscoderException transcoderException) {
                transcoderException.printStackTrace();
            }
            aboutStuffPanel.add(new JLabel(new ImageIcon(iconsvg.getImage())));
        aboutStuffPanel.add(new JLabel("            Baza sponsorów Festiwalu Robotyki Robocomp"));
        JOptionPane.showMessageDialog(MainFrame.this, aboutStuffPanel, "O programie", JOptionPane.INFORMATION_MESSAGE);
    }
        else if (e.getSource() == logOut) {
            this.dispose();
            try {
                PrintWriter outputFile = new PrintWriter("adds/token");
                outputFile.print("");
                outputFile.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            EventQueue.invokeLater(LoginFrame::new);
        } else if (e.getSource() == addCompany) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new InputDataFrame().setVisible(true);
                }
            });
            showMyCompanies();
        } else if (e.getSource() == myCompanies) {
            showMyCompanies();
        } else if (e.getSource() == showProfile)
        {
            if(this.showProfile.getText().equals("Pokaż"))
            {
                profile = new PersonalPanel(ServerCommunication.queries.me());
                this.add(profile,BorderLayout.EAST);
                this.showProfile.setText("Ukryj");
            }
            else{
                this.showProfile.setText("Pokaż");
                this.remove(profile);
            }
            showMyCompanies();
        }
    }

    public void showMyCompanies() {
        while (listModel.getComponentCount() > 0) {
            listModel.remove(listModel.getComponent(listModel.getComponentCount() - 1));
        }
        results = ServerCommunication.queries.companiesAll();

        for (CompanyNode node : results) {
            listModel.add(new CompanyPanel(node, this));
        }
        this.paintComponents(getGraphics());
    }

    private JMenuBar mb = new JMenuBar();

    private JMenu programMenu = new JMenu("Program");

    private JMenuItem closeProgram = new JMenuItem("Wyjdź");
    private JMenuItem help = new JMenuItem("Pomoc");
    private JMenuItem about = new JMenuItem("O programie");

    private JMenu companiesMenu = new JMenu("Firmy");

    private JMenuItem findCompany = new JMenuItem("Znajdź firmę");
    private JMenuItem addCompany = new JMenuItem("Dodaj sponsora");
    private JMenuItem myCompanies = new JMenuItem("Pokaż aktualną listę firm");

    private JMenu staffMenu = new JMenu("Staff");

    private JMenuItem editUsers = new JMenuItem("Edit Users");

    private JMenu departmentsMenu = new JMenu("Departments");
    private JMenu historyMenu = new JMenu("Contact History");
    private JMenu summaryMenu = new JMenu("Cooperation Summary");

    private JMenu userInfoMenu = new JMenu("Mój profil");

    private JMenuItem showProfile = new JMenuItem("Ukryj");
    private JMenuItem logOut = new JMenuItem("Wyloguj mnie");


    private JPanel mainPanel = new JPanel();

    private JPanel listModel;
    private JScrollPane listScrollPane;

    static ArrayList<CompanyNode> results;

    private PersonalPanel profile;
    private StaffNode me;

    private JPanel companyContentPanel;
    private FilterPanel filterPanel;

    private JPanel aboutStuffPanel;

    private File svgfile;
    private SVGIcon iconsvg;

    private static final long serialVersionUID = 1L;
}
