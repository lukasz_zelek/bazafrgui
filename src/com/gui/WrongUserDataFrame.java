package com.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class WrongUserDataFrame extends JDialog {
    public WrongUserDataFrame()
    {
        setResizable(false);
        this.setAlwaysOnTop(true);
        this.setModal(true);
        this.setModalityType(ModalityType.APPLICATION_MODAL);
        this.setUndecorated(true);
        initElements();

        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    void initElements(){
        this.add(mainPanel);
        mainPanel.setBackground(GlobalOptions.userInputColor);
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.black));

        mainPanel.add(dataPanel);
        dataPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        dataPanel.setBackground(GlobalOptions.userInputColor);

        dataPanel.setLayout(new BorderLayout());

        dataPanel.add(wrongData, BorderLayout.NORTH);
        dataPanel.add(oKbutton, BorderLayout.SOUTH);
        oKbutton.setBackground(GlobalOptions.buttonBackgroundColor);
        oKbutton.setForeground(GlobalOptions.buttonForegroundColor);
        oKbutton.addActionListener(this::actionPerformed);

    }

    //Panele
    private JPanel mainPanel = new JPanel();
    private JPanel dataPanel = new JPanel();

    //Elementy
    private JLabel wrongData = new JLabel("Błędne dane logowania");
    private JButton oKbutton = new JButton("OK");

    public void actionPerformed(ActionEvent e){
        if(e.getSource()==oKbutton){
            dispose();
        }
    }
}
