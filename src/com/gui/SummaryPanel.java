package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CooperationSummaryNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;

public class SummaryPanel extends JPanel {
    public SummaryPanel(CooperationSummaryNode cooperationSummaryNode, InputDataFrame parentFrame){
        this.parentFrame = parentFrame;
        this.cooperationSummaryNode = cooperationSummaryNode;
        this.setLayout(new GridLayout(5,1));
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        initGraphicalComponents();

        this.yearField.setText(this.cooperationSummaryNode.getYear());
        this.responsiblePersonField.setText(this.cooperationSummaryNode.getResponsiblePerson().getName() + " " + this.cooperationSummaryNode.getResponsiblePerson().getSurname());
        this.resultField.setText(this.cooperationSummaryNode.getResults());

        String dateString = this.cooperationSummaryNode.getEditTime();
        if(LocalDateTime.parse(dateString).getMinute() < 10){
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
        }
        else{
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
        }

        this.editDateField.setText(dateString);
    }

    public void initGraphicalComponents(){
        this.yearPanel = new JPanel();
        this.resultPanel = new JPanel();
        this.responsiblePersonPanel = new JPanel();
        this.buttonPanel = new JPanel();
        this.editDatePanel = new JPanel();

        this.add(this.editDatePanel);
        this.editDatePanel.setLayout(new BorderLayout());
        this.editDatePanel.add(this.editDateLabel, BorderLayout.NORTH);
        this.editDateField = new JTextField();
        this.editDateField.setEditable(false);
        this.editDatePanel.add(this.editDateField, BorderLayout.CENTER);

        this.add(this.yearPanel);
        this.yearPanel.setLayout(new BorderLayout());
        this.yearPanel.add(this.yearLabel, BorderLayout.NORTH);
        this.yearField = new JTextField();
        this.yearField.setEditable(false);
        this.yearPanel.add(this.yearField, BorderLayout.CENTER);

        this.add(this.responsiblePersonPanel);
        this.responsiblePersonPanel.setLayout(new BorderLayout());
        this.responsiblePersonPanel.add(this.responsiblePersonLabel, BorderLayout.NORTH);
        this.responsiblePersonField = new JTextField();
        this.responsiblePersonField.setEditable(false);
        this.responsiblePersonPanel.add(this.responsiblePersonField, BorderLayout.CENTER);

        this.add(this.resultPanel);
        this.resultPanel.setLayout(new BorderLayout());
        this.resultPanel.add(this.resultLabel, BorderLayout.NORTH);
        this.resultField = new JTextField();
        this.resultField.setEditable(false);
        this.resultPanel.add(this.resultField, BorderLayout.CENTER);

        this.add(this.buttonPanel);
        this.buttonPanel.setLayout(new FlowLayout());
        this.buttonPanel.add(this.editButton);
        this.buttonPanel.add(this.deleteButton);

        this.editButton.addActionListener(this::actionPerformed);
        this.deleteButton.addActionListener(this::actionPerformed);
    }

    private CooperationSummaryNode cooperationSummaryNode;

    private JPanel yearPanel;
    private JPanel responsiblePersonPanel;
    private JPanel resultPanel;
    private JPanel buttonPanel;
    private JPanel editDatePanel;

    private JTextField yearField;
    private JTextField responsiblePersonField;
    private JTextField resultField;
    private JTextField editDateField;

    private JLabel editDateLabel = new JLabel("Data raportu");
    private JLabel yearLabel = new JLabel("Edycja");
    private JLabel responsiblePersonLabel = new JLabel("Autor");
    private JLabel resultLabel = new JLabel("Wynik współpracy");

    private JButton editButton = new JButton("Szczegóły");
    private JButton deleteButton = new JButton("Usuń");

    private InputDataFrame parentFrame;

    private void actionPerformed(ActionEvent e){
        if(e.getSource() == deleteButton){
            ServerCommunication.mutations.cooperationSummaryDelete(this.cooperationSummaryNode.getId());
            this.parentFrame.showSummary();
        }
        else if(e.getSource() == editButton){
            new SummaryFrame(parentFrame.getId(), this.cooperationSummaryNode).setVisible(true);
        }

    }
}
