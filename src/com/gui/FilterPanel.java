package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CompanyNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

public class FilterPanel extends JPanel {

    public FilterPanel(MainFrame mainFrame, JPanel listModel)
    {
        this.setLayout(new GridLayout(3,9));
        initGraphicalComponents();

        this.mainFrame = mainFrame;
        this.listModel = listModel;
    }

    public void initGraphicalComponents(){
        sortButton = new JButton("Sortuj");
        sortButton.addActionListener(this::actionPerformed);
        sortComboBox = new JComboBox();
        sortComboBox.addItem("Nazwa");
        sortComboBox.addItem("Osoba odpowiedzialna");
        sortComboBox.addItem("Data");
        sortTypeComboBox = new JComboBox();
        sortTypeComboBox.addItem("rosnąco");
        sortTypeComboBox.addItem("malejąco");

        filterButton = new JButton("Wyszukaj");
        filterButton.addActionListener(this::actionPerformed);
        filterComboBox = new JComboBox();
        filterComboBox.addItem("Nazwa");

        filterTextField = new JTextField(20);
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel("Kategoria"));
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel("Kategoria"));
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(sortComboBox);
        this.add(sortTypeComboBox);
        this.add(sortButton);
        this.add(new JLabel());
        this.add(filterTextField);
        this.add(filterComboBox);
        this.add(filterButton);
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
    }

    private void actionPerformed(ActionEvent e) {
        if(e.getSource() == sortButton)
        {
            boolean ascendingType = sortTypeComboBox.getSelectedItem().toString().equals("rosnąco");
            sortList(sortComboBox.getSelectedItem().toString(), ascendingType);
        }else if(e.getSource() == filterButton)
        {
            Pattern pattern = Pattern.compile(filterTextField.getText(),Pattern.CASE_INSENSITIVE);
            filterList(pattern);
        }
    }

    private void filterList(Pattern pat)
    {
        MainFrame.results = ServerCommunication.queries.companiesAll();
        while (listModel.getComponentCount() > 0) {
            listModel.remove(listModel.getComponent(listModel.getComponentCount() - 1));
        }
        ArrayList<CompanyNode> new_array = new ArrayList<>();
        if(filterComboBox.getSelectedItem().toString().equals("Nazwa"))
        {
            for(CompanyNode node : MainFrame.results) {
                if(pat.matcher(node.getName()).find())
                    new_array.add(node);
            }
            MainFrame.results = new_array;
        }
        for (CompanyNode node : MainFrame.results) {
            listModel.add(new CompanyPanel(node, mainFrame));
        }
        mainFrame.paintComponents(mainFrame.getGraphics());
    }

    private void sortList(String criterion, boolean asc){
        while (listModel.getComponentCount() > 0) {
            listModel.remove(listModel.getComponent(listModel.getComponentCount() - 1));
        }
        if(criterion.equals("Nazwa"))
            MainFrame.results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
        else if(criterion.equals("Osoba odpowiedzialna"))
            MainFrame.results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    try{
                        String o1s = o1.getResponsiblePerson().getName()+o1.getResponsiblePerson().getSurname();
                        try{
                            String o2s = o2.getResponsiblePerson().getName()+o2.getResponsiblePerson().getSurname();
                            return o1s.compareToIgnoreCase(o2s);
                        }catch (NullPointerException a){
                            return 1;
                        }
                    }catch(NullPointerException a){
                        try{
                            String o2s = o2.getResponsiblePerson().getName()+o2.getResponsiblePerson().getSurname();
                            return -1;
                        }catch(NullPointerException b){
                            return 0;
                        }
                    }
                }
            });
        else if(criterion.equals("Data"))
            MainFrame.results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    try{
                        String o1t = o1.getContactHistory().getEdges().get(o1.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                        try{
                            String o2t = o2.getContactHistory().getEdges().get(o2.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                            LocalDateTime c1t = LocalDateTime.parse(o1t);
                            LocalDateTime c2t = LocalDateTime.parse(o2t);
                            return c1t.compareTo(c2t);
                        }catch(IndexOutOfBoundsException|NullPointerException a){
                            return 1;
                        }
                    }catch(IndexOutOfBoundsException|NullPointerException a){
                        try{
                            String o2t = o2.getContactHistory().getEdges().get(o2.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                            return -1;
                        }catch(IndexOutOfBoundsException|NullPointerException e){
                            return 0;
                        }
                    }
                }
            });
        if(!asc)
            Collections.reverse(MainFrame.results);
        for (CompanyNode node : MainFrame.results) {
            listModel.add(new CompanyPanel(node, mainFrame));
        }
        mainFrame.paintComponents(mainFrame.getGraphics());
    }

    private JButton sortButton;
    private JComboBox sortComboBox;
    private JComboBox sortTypeComboBox;

    private JButton filterButton;
    private JComboBox filterComboBox;
    private JTextField filterTextField;

    private MainFrame mainFrame;
    private JPanel listModel;
}
