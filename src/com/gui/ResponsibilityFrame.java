package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.StaffNode;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class ResponsibilityFrame extends JFrame {
    public ResponsibilityFrame(String companyid, InputDataFrame parentFrame){
        super("Osoba odpowiedzialna");
        this.setLayout(new FlowLayout());
        this.requestFocus();
        this.setAlwaysOnTop(true);
        this.parentFrame = parentFrame;
        this.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2-300, Toolkit.getDefaultToolkit().getScreenSize().height/2-50);
        this.setSize(600,100);
        mainPanel = new JPanel();
        this.companyId = companyid;
        staff = new JComboBox();
        ArrayList<StaffNode> staffNodes = ServerCommunication.queries.staffersAll();
        this.id_s = new HashMap<>();
        for(StaffNode node: staffNodes) {
            String s = String.format("%20s %20s %20s", node.getName(), node.getSurname(), GlobalOptions.rankToString.get(node.getRank()));
            staff.addItem(s);
            id_s.put(s,node.getId());
        }
        buttonApply = new JButton("Wybierz");
        buttonApply.addActionListener(this::actionPerformed);
        this.add(mainPanel);
        mainPanel.add(staff);
        mainPanel.add(buttonApply);
    }

    private void actionPerformed(ActionEvent e) {
        ServerCommunication.mutations.companySetResponsiblePerson(id_s.get(staff.getSelectedItem()),companyId);
        this.parentFrame.resetResponsibleField();
        dispose();
    }

    private Map<String,String> id_s;
    private JPanel mainPanel;
    private String companyId;
    private JComboBox staff;
    private JButton buttonApply;
    private InputDataFrame parentFrame;
}
