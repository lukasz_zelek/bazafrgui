package com.servercommunication;

import com.gui.GlobalOptions;
import com.servercommunication.graphobjects.CompanyNode;
import com.servercommunication.graphobjects.DepartmentNode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main testowy. Do testowania zapytań i innych gówien do których inicjalizacja gui jest niepotrzebna
 */
public class TestMainClass {
    public static void main(String[] args) throws ParseException {
        //Loguje się, konieczne na później
        if(ServerCommunication.queries.signIn("matueszek123s4", "haslo123", true)) System.out.println("Logged");
        else System.out.println("nie");

        ServerCommunication.mutations.staffCreate(ServerCommunication.queries.departmentsAll().get(0).getName(),"placeholder@test.pl","Łukasz", "haslo123", GlobalOptions.RANKS.ADMIN.toString(), 2018,"Zelek","lzelek");
    }
}
