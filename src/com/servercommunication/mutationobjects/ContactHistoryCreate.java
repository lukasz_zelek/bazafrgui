package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.ContactHistoryNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name = "contactHistoryCreate",
        arguments = {
                @GraphQLArgument(name = "contactDate", type = "DateTime"),
                @GraphQLArgument(name = "companyId"),
                @GraphQLArgument(name = "notes")
        }
)
public class ContactHistoryCreate {
    private ContactHistoryNode contactHistory;

    public ContactHistoryNode getContactHistory() {
        return contactHistory;
    }

    public void setContactHistory(ContactHistoryNode contactHistory) {
        this.contactHistory = contactHistory;
    }
}
