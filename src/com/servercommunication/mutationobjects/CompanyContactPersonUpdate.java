package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.ContactPersonNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companyContactPersonUpdate",
        arguments={
                @GraphQLArgument(name="contactPersonId"),
                @GraphQLArgument(name="email"),
                @GraphQLArgument(name="name"),
                @GraphQLArgument(name="notes"),
                @GraphQLArgument(name="phoneNumber")
        }
)
public class CompanyContactPersonUpdate {
    private ContactPersonNode contactPersonNode;

    public ContactPersonNode getContactPersonNode() {
        return contactPersonNode;
    }

    public void setContactPersonNode(ContactPersonNode contactPersonNode) {
        this.contactPersonNode = contactPersonNode;
    }
}
