package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.ContactHistoryNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;


@GraphQLProperty(
        name = "contactHistoryUpdate",
        arguments = {
                @GraphQLArgument(name = "contactDate"),
                @GraphQLArgument(name = "contactHistoryId"),
                @GraphQLArgument(name = "notes")
        }
)
public class ContactHistoryUpdate {
    private ContactHistoryNode contactHistory;

    public ContactHistoryNode getContactHistory() {
        return contactHistory;
    }

    public void setContactHistory(ContactHistoryNode contactHistory) {
        this.contactHistory = contactHistory;
    }
}
