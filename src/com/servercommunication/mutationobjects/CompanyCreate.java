package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.CompanyNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companyCreate",
        arguments={
                @GraphQLArgument(name="name"),
                @GraphQLArgument(name="description"),
                @GraphQLArgument(name="email"),
                @GraphQLArgument(name="phoneNumber"),
                @GraphQLArgument(name="www"),
                @GraphQLArgument(name="potential", type="int")
        }
)
public class CompanyCreate {
    private CompanyNode company;

    public CompanyNode getCompany() {
        return company;
    }

    public void setCompany(CompanyNode company) {
        this.company = company;
    }
}
