package com.servercommunication.mutationobjects;


import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name = "cooperationSummaryDelete",
        arguments = {
                @GraphQLArgument(name = "cooperationSummaryId")
        }
)
public class CooperationSummaryDelete {
    private String cooperationSummaryId;

    public String getCooperationSummaryId() {
        return cooperationSummaryId;
    }

    public void setCooperationSummaryId(String cooperationSummaryId) {
        this.cooperationSummaryId = cooperationSummaryId;
    }
}
