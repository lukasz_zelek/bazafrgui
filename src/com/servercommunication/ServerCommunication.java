package com.servercommunication;

/**
 * Klasa odpowiedzialna za komunikację z serwerem. Pole queries zawiera zapytania, pole mutations mutacje
 */
public class ServerCommunication {          
    public static Mutations mutations = new Mutations();
    public static Queries queries = new Queries();
}
