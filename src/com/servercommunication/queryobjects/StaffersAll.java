package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.StaffNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;


@GraphQLProperty(name="staffersAll")
public class StaffersAll extends StaffNodeConnection {
}
