package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.ContactHistoryNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name = "contactHistoryByCompany",
        arguments ={
        @GraphQLArgument(name = "companyId")
}
)
public class ContactHistoryByCompany extends ContactHistoryNodeConnection {
}
