package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.CooperationSummaryNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="cooperationSummaryByYear",
        arguments = {
                @GraphQLArgument(name = "year")
        }
)
public class CooperationSummaryByYear extends CooperationSummaryNodeConnection {
}
