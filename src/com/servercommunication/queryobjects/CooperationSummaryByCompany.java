package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.CooperationSummaryNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name="cooperationSummaryByCompany",
        arguments = {
        @GraphQLArgument(name = "companyId")
        }
)
public class CooperationSummaryByCompany extends CooperationSummaryNodeConnection {
}
