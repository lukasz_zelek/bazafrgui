package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.AuthData;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;


/**
 * Obiekt zapytania, ma zawierać pola które mają być zwrócone w zapytaniu
 * w anotacjach deklarujesz argumenty o jakie pyta
 * a także nazwę query. Musi być taka jak nazwa zapytania
 *
 * Pozostałe klasy zapytań robić analogicznie
 */

@GraphQLProperty(
                name="signIn",
                arguments ={
                @GraphQLArgument(name = "username"),
                @GraphQLArgument(name = "password"),
                @GraphQLArgument(name = "generateToken"),
                @GraphQLArgument(name = "rememberMe")
        }
)
public class SignIn extends AuthData {

}
