package com.servercommunication.graphobjects;

import com.gui.GlobalOptions.RANKS;

public class StaffNode implements Node {
    private String name;
    private String surname;
    private boolean active;
    private int studyYearStart;
    private RANKS rank;
    private String email;
    private String id;
    private DepartmentNode department;

    public DepartmentNode getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentNode department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getStudyYearStart() {
        return studyYearStart;
    }

    public void setStudyYearStart(int studyYearStart) {
        this.studyYearStart = studyYearStart;
    }

    public RANKS getRank() {
        return rank;
    }

    public void setRank(RANKS rank) {
        this.rank = rank;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getId() {
        return id;
    }
}
