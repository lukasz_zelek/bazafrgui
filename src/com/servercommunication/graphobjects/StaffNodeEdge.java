package com.servercommunication.graphobjects;

public class StaffNodeEdge {
    private StaffNode node;
    private String cursor;

    public StaffNode getNode() {
        return node;
    }

    public void setNode(StaffNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
