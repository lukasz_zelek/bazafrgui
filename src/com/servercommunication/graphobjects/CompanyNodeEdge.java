package com.servercommunication.graphobjects;

public class CompanyNodeEdge {
    private CompanyNode node;
    private String cursor;

    public CompanyNode getNode() {
        return node;
    }

    public void setNode(CompanyNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
