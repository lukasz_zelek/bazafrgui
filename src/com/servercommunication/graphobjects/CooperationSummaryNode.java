package com.servercommunication.graphobjects;

public class CooperationSummaryNode implements Node {
    private String year;
    private String results;
    private String summary;
    private int responsiblePersonId;
    private CompanyNode company;
    private StaffNode responsiblePerson;
    private String id;
    private String editTime;

    public String getEditTime() {
        return editTime;
    }

    public void setEditTime(String editTime) {
        this.editTime = editTime;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getResponsiblePersonId() {
        return responsiblePersonId;
    }

    public void setResponsiblePersonId(int responsiblePersonId) {
        this.responsiblePersonId = responsiblePersonId;
    }

    public CompanyNode getCompany() {
        return company;
    }

    public void setCompany(CompanyNode company) {
        this.company = company;
    }

    public StaffNode getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(StaffNode responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    @Override
    public String getId() {
        return id;
    }
}
