package com.servercommunication.graphobjects;

import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="department"
)
public class DepartmentNode implements Node {
    private String name;
    //private StaffNode supervisor;
    //private StaffNodeConnection departmentsStaff;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**

    public StaffNode getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(StaffNode supervisor) {
        this.supervisor = supervisor;
    }

    public StaffNodeConnection getDepartmentsStaff() {
        return departmentsStaff;
    }

    public void setDepartmentsStaff(StaffNodeConnection departmentsStaff) {
        this.departmentsStaff = departmentsStaff;
    }
     **/

    @Override
    public String getId() {
        return id;
    }
}
