package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class ContactHistoryNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<ContactHistoryNodeEdge> edges;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<ContactHistoryNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<ContactHistoryNodeEdge> edges) {
        this.edges = edges;
    }
}
