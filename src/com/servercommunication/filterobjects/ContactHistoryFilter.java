package com.servercommunication.filterobjects;

import java.util.Date;

public class ContactHistoryFilter {
    private Date contactDate;
    private Date contactDateNe;
    private Date contactDateLt;
    private Date contactDateLte;
    private Date contactDateGt;
    private Date contactDateGte;

    private IntRange contactDateRange;

    private Boolean contactDateToday;
    private Boolean contactDateThisWeek;
    private Boolean contactDateThisMonth;
    private Boolean contactDateThisYear;

    private ContactHistoryFilter[] and;

    private ContactHistoryFilter[] or;

    private ContactHistoryFilter not;

    public Date getContactDate() {
        return contactDate;
    }

    public void setContactDate(Date contactDate) {
        this.contactDate = contactDate;
    }

    public Date getContactDateNe() {
        return contactDateNe;
    }

    public void setContactDateNe(Date contactDateNe) {
        this.contactDateNe = contactDateNe;
    }

    public Date getContactDateLt() {
        return contactDateLt;
    }

    public void setContactDateLt(Date contactDateLt) {
        this.contactDateLt = contactDateLt;
    }

    public Date getContactDateLte() {
        return contactDateLte;
    }

    public void setContactDateLte(Date contactDateLte) {
        this.contactDateLte = contactDateLte;
    }

    public Date getContactDateGt() {
        return contactDateGt;
    }

    public void setContactDateGt(Date contactDateGt) {
        this.contactDateGt = contactDateGt;
    }

    public Date getContactDateGte() {
        return contactDateGte;
    }

    public void setContactDateGte(Date contactDateGte) {
        this.contactDateGte = contactDateGte;
    }

    public IntRange getContactDateRange() {
        return contactDateRange;
    }

    public void setContactDateRange(IntRange contactDateRange) {
        this.contactDateRange = contactDateRange;
    }

    public Boolean getContactDateToday() {
        return contactDateToday;
    }

    public void setContactDateToday(Boolean contactDateToday) {
        this.contactDateToday = contactDateToday;
    }

    public Boolean getContactDateThisWeek() {
        return contactDateThisWeek;
    }

    public void setContactDateThisWeek(Boolean contactDateThisWeek) {
        this.contactDateThisWeek = contactDateThisWeek;
    }

    public Boolean getContactDateThisMonth() {
        return contactDateThisMonth;
    }

    public void setContactDateThisMonth(Boolean contactDateThisMonth) {
        this.contactDateThisMonth = contactDateThisMonth;
    }

    public Boolean getContactDateThisYear() {
        return contactDateThisYear;
    }

    public void setContactDateThisYear(Boolean contactDateThisYear) {
        this.contactDateThisYear = contactDateThisYear;
    }

    public ContactHistoryFilter[] getAnd() {
        return and;
    }

    public void setAnd(ContactHistoryFilter[] and) {
        this.and = and;
    }

    public ContactHistoryFilter[] getOr() {
        return or;
    }

    public void setOr(ContactHistoryFilter[] or) {
        this.or = or;
    }

    public ContactHistoryFilter getNot() {
        return not;
    }

    public void setNot(ContactHistoryFilter not) {
        this.not = not;
    }
}
