type AuthData {
    authToken: String
}

type CompanyCreate {
    company: CompanyNode
}

type CompanyDelete {
    companyId: ID
}

input CompanyFilter {
    """Exact match."""
    name: String

    """Not match."""
    nameNe: String

    """In a given list."""
    nameIn: [String!]

    """Case-sensitive containment test."""
    nameLike: String

    """Case-insensitive containment test."""
    nameIlike: String

    """Exact match."""
    email: String

    """Not match."""
    emailNe: String

    """In a given list."""
    emailIn: [String]

    """Case-sensitive containment test."""
    emailLike: String

    """Case-insensitive containment test."""
    emailIlike: String

    """Exact match."""
    www: String

    """Not match."""
    wwwNe: String

    """In a given list."""
    wwwIn: [String]

    """Case-sensitive containment test."""
    wwwLike: String

    """Case-insensitive containment test."""
    wwwIlike: String

    """Exact match."""
    phoneNumber: String

    """Not match."""
    phoneNumberNe: String

    """Case-sensitive containment test."""
    phoneNumberLike: String

    """Case-insensitive containment test."""
    phoneNumberIlike: String

    """Exact match."""
    potential: Int

    """Less than."""
    potentialLt: Int

    """Less than or equal to."""
    potentialLte: Int

    """Greater than."""
    potentialGt: Int

    """Greater than or equal to."""
    potentialGte: Int

    """Not match."""
    potentialNe: Int

    """In a given list."""
    potentialIn: [Int!]

    """Not in a given list."""
    potentialNotIn: [Int!]

    """Selects values within a given range."""
    potentialRange: IntRange

    """Conjunction of filters joined by ``AND``."""
    and: [CompanyFilter!]

    """Conjunction of filters joined by ``OR``."""
    or: [CompanyFilter!]

    """Negation of filters."""
    not: CompanyFilter
}

type CompanyNode implements Node {
    name: String!
    email: String
    www: String
    phoneNumber: String
    potential: Int!
    description: String
    responsiblePerson: StaffNode

    """The ID of the object."""
    id: ID!
}

type CompanyNodeConnection {
    """Pagination data for this connection."""
    pageInfo: PageInfo!

    """Contains the nodes in this connection."""
    edges: [CompanyNodeEdge]!
}

"""A Relay edge containing a `CompanyNode` and its cursor."""
type CompanyNodeEdge {
    """The item at the end of the edge"""
    node: CompanyNode

    """A cursor for use in pagination"""
    cursor: String!
}

type CompanySetResponsiblePerson {
    company: CompanyNode
}

type CompanyUpdate {
    company: CompanyNode
}

type ContactHistoryCreate {
    contactHistory: ContactHistoryNode
}

type ContactHistoryDelete {
    contactHistoryId: ID
}

input ContactHistoryFilter {
    """Exact match."""
    contactDate: DateTime

    """Not match."""
    contactDateNe: DateTime

    """Less than."""
    contactDateLt: DateTime

    """Less than or equal to."""
    contactDateLte: DateTime

    """Greater than."""
    contactDateGt: DateTime

    """Greater than or equal to."""
    contactDateGte: DateTime

    """Selects values within a given range."""
    contactDateRange: DateTimeRange
    contactDateToday: Boolean
    contactDateThisWeek: Boolean
    contactDateThisMonth: Boolean
    contactDateThisYear: Boolean

    """Conjunction of filters joined by ``AND``."""
    and: [ContactHistoryFilter!]

    """Conjunction of filters joined by ``OR``."""
    or: [ContactHistoryFilter!]

    """Negation of filters."""
    not: ContactHistoryFilter
}

type ContactHistoryNode implements Node {
    notes: String
    contactDate: DateTime!
    company: CompanyNode
    representative: StaffNode

    """The ID of the object."""
    id: ID!
}

type ContactHistoryNodeConnection {
    """Pagination data for this connection."""
    pageInfo: PageInfo!

    """Contains the nodes in this connection."""
    edges: [ContactHistoryNodeEdge]!
}

"""A Relay edge containing a `ContactHistoryNode` and its cursor."""
type ContactHistoryNodeEdge {
    """The item at the end of the edge"""
    node: ContactHistoryNode

    """A cursor for use in pagination"""
    cursor: String!
}

type ContactHistoryUpdate {
    contactHistory: ContactHistoryNode
}

type CooperationSummaryCreate {
    cooperationSummary: CooperationSummaryNode
}

type CooperationSummaryDelete {
    cooperationSummaryId: ID
}

input CooperationSummaryFilter {
    """Exact match."""
    year: ID

    """Not match."""
    yearNe: ID

    """Less than."""
    yearLt: ID

    """Less than or equal to."""
    yearLte: ID

    """Greater than."""
    yearGt: ID

    """Greater than or equal to."""
    yearGte: ID

    """Selects values within a given range."""
    yearRange: IDRange

    """Conjunction of filters joined by ``AND``."""
    and: [CooperationSummaryFilter!]

    """Conjunction of filters joined by ``OR``."""
    or: [CooperationSummaryFilter!]

    """Negation of filters."""
    not: CooperationSummaryFilter
}

type CooperationSummaryNode implements Node {
    year: ID!
    results: String
    summary: String!
    responsiblePersonId: Int
    company: CompanyNode
    responsiblePerson: StaffNode

    """The ID of the object."""
    id: ID!
}

type CooperationSummaryNodeConnection {
    """Pagination data for this connection."""
    pageInfo: PageInfo!

    """Contains the nodes in this connection."""
    edges: [CooperationSummaryNodeEdge]!
}

"""A Relay edge containing a `CooperationSummaryNode` and its cursor."""
type CooperationSummaryNodeEdge {
    """The item at the end of the edge"""
    node: CooperationSummaryNode

    """A cursor for use in pagination"""
    cursor: String!
}

type CooperationSummaryUpdate {
    cooperationSummary: CooperationSummaryNode
}

"""
The `DateTime` scalar type represents a DateTime
value as specified by
[iso8601](https://en.wikipedia.org/wiki/ISO_8601).
"""
scalar DateTime

input DateTimeRange {
    begin: DateTime!
    end: DateTime!
}

type DepartmentCreate {
    department: DepartmentNode
}

type DepartmentDelete {
    departmentId: ID
}

input DepartmentFilter {
    """Exact match."""
    name: String

    """Not match."""
    nameNe: String

    """In a given list."""
    nameIn: [String!]

    """Case-sensitive containment test."""
    nameLike: String

    """Case-insensitive containment test."""
    nameIlike: String

    """Conjunction of filters joined by ``AND``."""
    and: [DepartmentFilter!]

    """Conjunction of filters joined by ``OR``."""
    or: [DepartmentFilter!]

    """Negation of filters."""
    not: DepartmentFilter
}

type DepartmentNode implements Node {
    name: String!
    supervisor: StaffNode
    departmentsStaff(before: String, after: String, first: Int, last: Int): StaffNodeConnection

    """The ID of the object."""
    id: ID!
}

type DepartmentNodeConnection {
    """Pagination data for this connection."""
    pageInfo: PageInfo!

    """Contains the nodes in this connection."""
    edges: [DepartmentNodeEdge]!
}

"""A Relay edge containing a `DepartmentNode` and its cursor."""
type DepartmentNodeEdge {
    """The item at the end of the edge"""
    node: DepartmentNode

    """A cursor for use in pagination"""
    cursor: String!
}

type DepartmentSetSupervisor {
    department: DepartmentNode
}

input IDRange {
    begin: ID!
    end: ID!
}

input IntRange {
    begin: Int!
    end: Int!
}

type Mutation {
    staffCreate(department: String!, email: String!, name: String!, password: String, rank: String, studyYearStart: Int, surname: String!, username: String!): StaffCreate
    staffChangeDepartment(department: String!, staffId: ID!): StaffChangeDepartment
    staffChangeRank(rank: String!, staffId: String!): StaffChangeRank
    departmentCreate(name: String!): DepartmentCreate
    departmentDelete(departmentId: ID!): DepartmentDelete
    departmentSetSupervisor(departmentId: ID!, staffId: String): DepartmentSetSupervisor
    companyCreate(description: String, email: String, name: String!, phoneNumber: String, potential: Int, www: String): CompanyCreate
    companyDelete(companyId: ID!): CompanyDelete
    companyUpdate(companyId: ID!, description: String, email: String, phoneNumber: String, potential: Int, www: String): CompanyUpdate
    companySetResponsiblePerson(companyId: ID!, staffId: ID): CompanySetResponsiblePerson
    contactHistoryCreate(companyId: ID!, contactDate: DateTime!, notes: String!): ContactHistoryCreate
    contactHistoryDelete(contactHistoryId: ID!): ContactHistoryDelete
    contactHistoryUpdate(contactDate: DateTime, contactHistoryId: ID!, notes: String): ContactHistoryUpdate
    cooperationSummaryCreate(companyId: ID!, results: String!, summary: String, year: Int!): CooperationSummaryCreate
    cooperationSummaryDelete(cooperationSummaryId: ID!): CooperationSummaryDelete
    cooperationSummaryUpdate(cooperationSummaryId: ID!, results: String, summary: String): CooperationSummaryUpdate
}

"""An object with an ID"""
interface Node {
    """The ID of the object."""
    id: ID!
}

"""
The Relay compliant `PageInfo` type, containing data necessary to paginate this connection.
"""
type PageInfo {
    """When paginating forwards, are there more items?"""
    hasNextPage: Boolean!

    """When paginating backwards, are there more items?"""
    hasPreviousPage: Boolean!

    """When paginating backwards, the cursor to continue."""
    startCursor: String

    """When paginating forwards, the cursor to continue."""
    endCursor: String
}

type Query {
    me: StaffNode
    signIn(username: String!, password: String!, generateToken: Boolean!, rememberMe: Boolean!): AuthData
    staffersAll(filters: StaffFilter, before: String, after: String, first: Int, last: Int): StaffNodeConnection
    departmentsAll(filters: DepartmentFilter, before: String, after: String, first: Int, last: Int): DepartmentNodeConnection
    companiesAll(filters: CompanyFilter, before: String, after: String, first: Int, last: Int): CompanyNodeConnection
    contactHistoryByCompany(companyId: ID!, filters: ContactHistoryFilter, before: String, after: String, first: Int, last: Int): ContactHistoryNodeConnection
    cooperationSummaryByCompany(companyId: ID!, filters: CooperationSummaryFilter, before: String, after: String, first: Int, last: Int): CooperationSummaryNodeConnection
    cooperationSummaryByYear(year: Int!, filters: CooperationSummaryFilter, before: String, after: String, first: Int, last: Int): CooperationSummaryNodeConnection
}

"""An enumeration."""
enum RANKS {
    MAIN_ORGANIZER
    SECTION_COORDINATOR
    NORMAL_WORKER
    VOLUNTEER
    ADMIN
}

type StaffChangeDepartment {
    staff: StaffNode
}

type StaffChangeRank {
    staff: StaffNode
}

type StaffCreate {
    staff: StaffNode
}

input StaffFilter {
    """Exact match."""
    name: String

    """Not match."""
    nameNe: String

    """In a given list."""
    nameIn: [String!]

    """Case-sensitive containment test."""
    nameLike: String

    """Case-insensitive containment test."""
    nameIlike: String

    """Exact match."""
    surname: String

    """Not match."""
    surnameNe: String

    """In a given list."""
    surnameIn: [String!]

    """Case-sensitive containment test."""
    surnameLike: String

    """Case-insensitive containment test."""
    surnameIlike: String

    """Exact match."""
    active: Boolean

    """Not match."""
    activeNe: Boolean

    """Conjunction of filters joined by ``AND``."""
    and: [StaffFilter!]

    """Conjunction of filters joined by ``OR``."""
    or: [StaffFilter!]

    """Negation of filters."""
    not: StaffFilter
}

type StaffNode implements Node {
    name: String!
    surname: String!
    active: Boolean!
    studyYearStart: Int
    rank: RANKS!
    email: String!
    department: DepartmentNode
    responsibleFor(before: String, after: String, first: Int, last: Int): CompanyNodeConnection

    """The ID of the object."""
    id: ID!
}

type StaffNodeConnection {
    """Pagination data for this connection."""
    pageInfo: PageInfo!

    """Contains the nodes in this connection."""
    edges: [StaffNodeEdge]!
}

"""A Relay edge containing a `StaffNode` and its cursor."""
type StaffNodeEdge {
    """The item at the end of the edge"""
    node: StaffNode

    """A cursor for use in pagination"""
    cursor: String!
}

